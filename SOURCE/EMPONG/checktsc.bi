REM
REM * This file is part of the Emeritus Pong project *
REM
REM Copyright (C) Mateusz Viste 2010
REM
REM This function checks whether the CPU supports the RDTSC instruction
REM or not. Returns 1 if RDTSC is supported, 0 otherwise.
REM

FUNCTION CheckTSC() AS BYTE
      DIM AS INTEGER i
      ' CPUID supported if can set/clear ID flag (EFLAGS bit 21).
      ASM
        pushfd
        pop edx
        pushfd
        pop eax
        Xor eax, &h200000  '' flip ID flag
        push eax
        popfd
        pushfd
        pop eax
        Xor eax, edx
        mov [i], eax
      END ASM
      IF i = 0 THEN RETURN 0

      ' TSC supported if CPUID func 1 returns with bit 4 of EDX set.
      ASM
        mov eax, 1
        cpuid
        And edx, &h10
        mov [i], edx
      END ASM
      IF i = 0 THEN RETURN 0 ELSE RETURN 1
END FUNCTION
